import { Component, OnInit } from '@angular/core';
import { HobbyImageProviderService } from './hobby-image-provider.service';
import { HobbyInfoProviderService } from './hobby-info-provider.service';
import { HobbyModel } from "./hobbyModel";
import { ImageModel } from './imageModel';

@Component({
  selector: 'app-hobby',
  templateUrl: './hobby.component.html',
  styleUrls: ['./hobby.component.css']
})
export class HobbyComponent implements OnInit {
  hobby: HobbyModel;
  images: ImageModel[] = [];
  
  editHobbyName: string;
  editHobbyDescription: string;

  constructor(hobbyInfoProvider: HobbyInfoProviderService, private imageProvider: HobbyImageProviderService) {
    this.hobby = new HobbyModel();
    this.hobby.name = hobbyInfoProvider.name;
    this.hobby.description = hobbyInfoProvider.description;
    this.images = imageProvider.images;

    this.resetEditFields();
  }

  ngOnInit(): void {
  }


  resetEditFields() {
    this.editHobbyName = this.hobby.name;
    this.editHobbyDescription = this.hobby.description;
  }

  saveEdit() {
    this.hobby.name = this.editHobbyName;
    this.hobby.description = this.editHobbyDescription;
  }
}
