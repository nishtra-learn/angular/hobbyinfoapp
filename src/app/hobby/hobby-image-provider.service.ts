import { Injectable } from '@angular/core';
import { ImageModel } from './imageModel';

@Injectable({
  providedIn: 'root'
})
export class HobbyImageProviderService {
  images: ImageModel[] = [];

  constructor() { 
    this.images.push({
      href: 'https://www.tynker.com/projects/images/5a4b0a1cc48d3a442ca3470c02ee30f2bc5a94de/splash---splash.png', 
      alt: 'Cookie Clicker', 
      caption: 'Cookie Clicker. Родоначальник жанра'
    });
    this.images.push({
      href: 'https://www.pcgamesn.com/wp-content/uploads/2019/03/adventure-capitalist-900x562.jpg', 
      alt: 'Adventure capitalist', 
      caption: 'Adventure Capitalist. Числа могут расти до совсем страшных значений'
    });
    this.images.push({
      href: 'https://www.pcgamesn.com/wp-content/uploads/2019/03/best-clicker-games-crusaders-of-the-lost-idols-900x506.jpg', 
      alt: 'Crusaders of the Lost Idols', 
      caption: 'Crusaders of the Lost Idols'
    });
  }
}
